import * as React from "react";
import "./App.css";

import logo from "./logo.svg";

interface IState {
  fields: IField[];
}

interface IField {
  description: string;
  isSelected: boolean;
  name: string;
  selectedOrder: number;
  selectedAlias?: string;
}

const fields: IField[] = [
  {
    description: "Name",
    isSelected: false,
    name: "name",
    selectedOrder: 0
  },
  {
    description: "Phone Number",
    isSelected: true,
    name: "phone",
    selectedOrder: 0
  },
  {
    description: "Email",
    isSelected: false,
    name: "email",
    selectedOrder: 0
  },
  {
    description: "Address",
    isSelected: false,
    name: "address",
    selectedOrder: 0
  }
];

class App extends React.Component<{}, IState> {
  public state: IState = {
    fields
  };

  public render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to Futurify</h1>
        </header>
        <div className="row">
          <div className="col-6">
            <div>
              Available fields <button>Select all</button>
            </div>
            {this.state.fields
              .filter(field => {
                return !field.isSelected;
              })
              .map((field, idx) => {
                return (
                  <div key={idx} className="available-field">
                    <label>{field.description}</label> <button>Select</button>
                  </div>
                );
              })}
          </div>
          <br />
          <hr />
          <div className="col-6">
            <div>
              Selected fields <button>Remove all</button>
            </div>
            {this.state.fields
              .filter(field => {
                return field.isSelected;
              })
              .map((field, idx) => {
                return (
                  <div key={idx} className="selected-field">
                    <input value={field.description} />
                    <button>Up</button>
                    <button>Down</button>
                    <button>Remove</button>
                  </div>
                );
              })}
          </div>
        </div>
      </div>
    );
  }
}

export default App;
